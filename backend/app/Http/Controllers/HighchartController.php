<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Order;
use DB;

class HighchartController extends Controller
{
    public function yearSalles()
    {
        $yearData = Order::select(DB::raw('SUM(total_cost) as count'))
                    ->whereYear('created_at', date('Y'))
                    ->groupBy(DB::raw("MONTH(created_at)"))
                    ->pluck('count');
          
        return view('orders.statistic', compact('yearData'));
    }
}
