<?php
  
namespace App\Http\Controllers;
   
use App\Models\Order;
use App\Models\Product;
use App\Models\OrderProduct;
use Illuminate\Http\Request;
use DB;
  
class OrderController extends Controller
{
    function __construct(){
        $this->middleware('permission:order-list|order-create|order-edit|order-delete', ['only' => ['index', 'show']]);
        $this->middleware('permission:order-create', ['only' => ['create','store']]);
        $this->middleware('permission:order-edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:order-delete', ['only' => ['destroy']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $orders = Order::join('users', 'orders.user_id', '=', 'users.id')
        ->get(['orders.id','users.name','orders.total_cost']);
    
        return view('orders.index',compact('orders'));
    }
     
    /**
     * Show the form for creating a new resource.
     *
     * @param \App\Product $product
     * @return \Illuminate\Http\Response
     * @param  \Illuminate\Http\Request  $request
     */
    public function create()
    {
        $products = Product::select('products.id','products.name','products.price')->get();
        return view('orders.create', compact('products'));
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $products = Product::select('products.id','products.price')
            ->get();

        $input = $request->all();
        $user = Auth()->user()->id;
        $total_cost = 0;
        $prods = [];
        
        foreach($input['products'] as $row => $innerArray){
                $prods[$innerArray] = (int)$input[$innerArray];
        }
        foreach($prods as $prod => $count){
            foreach($products as $product){
                if($product->id === $prod){
                    $total_cost += $product->price*$count;
                }
            }
        }
        $order = Order::create([
            'user_id' => $user,
            'total_cost' => $total_cost,
        ]);
        foreach($prods as $product => $count){
            OrderProduct::create([
                'order_id' => $order->id,
                'product_id' => $product,
                'product_count' => $count,
           ]);
        }
        

        return redirect()->route('orders.index')
                        ->with('success','Order created successfully');
    }
     
    /**
     * Display the specified resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function show(Order $order)
    {
        $order = Order::join('orders_products','orders_products.order_id','=','orders.id')
            ->join('products','orders_products.product_id','=','products.id')
            ->select('orders.id','products.name','orders_products.product_count')
            ->where('orders.id','=',$order->id)
            ->get();

        return view('orders.show',compact('order'));
    } 
     
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function edit(Order $order)
    {
        return view('orders.edit',compact('order'));
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Order $order)
    {
        $request->validate([
            'user_id' => 'required',
            'total_cost' => 'required',
        ]);
    
        $order->update($request->all());
    
        return redirect()->route('orders.index')
                        ->with('success','Order updated successfully');
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function destroy(Order $order)
    {
        $order->delete();
    
        return redirect()->route('orders.index')
                        ->with('success','Order deleted successfully');
    }

    public function sallerSales(){
        $orders = Order::join('orders_products','orders_products.order_id','=','orders.id')
        ->join('users','users.id','=','orders.user_id')
        ->join('products','orders_products.product_id','=','products.id')
        ->get(['users.id','orders_products.product_count','products.name','orders.total_cost',]);

        return view('orders.sallerSales',compact('orders'));
    }

    public function history(){
        $orders = Order::join('users','users.id','=','orders.user_id')
            ->select('orders.id','users.name','orders.total_cost')
            ->get();

        return view('orders.history',compact('orders'));
    }
}