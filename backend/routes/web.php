<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\ChangePasswordController;
use App\Http\Controllers\HighchartController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes(['register' => false]);

Route::get('/', function () {
    return view('welcome');
});
Auth::routes();
Route::group(['middleware' => ['auth']], function(){
    Route::get('/home', function(){
    return view('welcome');
    });
    Route::get('/change-password', [ChangePasswordController::class, 'index']);
    Route::post('/change-password', [ChangePasswordController::class, 'changePassword'])->name('change-password');
    Route::resource('orders', OrderController::class);
    Route::get('/sales/history', [OrderController::class, 'history'])->name('history');
});

Route::group(['middleware'=>['auth', 'role:admin']],function(){
    Route::resource('users', UserController::class);
    Route::get('/sales', [OrderController::class, 'sallerSales'])->name('sales');
    Route::resource('products', ProductController::class);
    Route::resource('roles', RoleController::class);
    Route::get('/sales/chart', [HighchartController::class,'yearSalles'])->name('statistic');
});