@extends('layouts.app')
@section('content')
@php
$array =[];
$money = 0;
$products = 0;
foreach($orders as $order){
        if(array_key_exists($order->name, $array)){
            $array[$order->name]+= $order->product_count;
        }else{
            $array[$order->name] = $order->product_count;
        }
        $money+= $order->total_cost;
        $products += $order->product_count;
}

@endphp
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="d-flex justify-content-center">
                <h2>Sales</h2>
            </div>
            
        </div>
    </div>
   
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
    <div class="row justify-content-center">
        <div class="card col-6">
            <div class='card-header'>Extra options</div>
            <div class="card-body ">
                <a class="btn btn-light" type="button" href="{{ route('history') }}">Orders</a>
                <a class="btn btn-light" type="button" href="{{ route('statistic') }}">Statistics</a>
            </div>
        </div>
    </div>

   <table class="table table-bordered ">
        <h2 class="d-flex justify-content-center">Summary</h2>
        <tr>
            <th>Money:</th>
            <th>Products selled</th>
        </tr>
        <tr>
            <td>{{ $money }}</td>
            <td>{{ $products }}</td>
        </tr>
    </table>
    <table class="table table-bordered">
        <h2 class="d-flex justify-content-center">Products selled</h2>
        <tr>
            <th>Product name</th>
            <th>Product count</th>
        </tr>
        
        @foreach ($array as $row=>$key)
        <tr>
            <td>{{ $row }}</td>
            <td>{{ $key }}</td>
        </tr>
        @endforeach
    </table>
    
   
      
@endsection