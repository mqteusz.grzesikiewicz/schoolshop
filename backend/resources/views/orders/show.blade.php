@extends('layouts.app')
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="d-flex justify-content-center">
                <h2>Show order</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('orders.index') }}"> Back</a>
            </div>
        </div>
    </div>
   
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
   
    <table class="table table-bordered">
        <tr>
            <th>Product name</th>
            <th>Product count</th>
        </tr>
        
        @foreach ($order as $data)
        <tr>
            <td>{{ $data->name }} </td>
            <td>{{ $data->product_count}}</td>
        </tr>
        @endforeach
    </table>

      
@endsection