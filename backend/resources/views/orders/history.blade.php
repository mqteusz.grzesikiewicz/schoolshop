@extends('layouts.app')
@section('content')

    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="d-flex justify-content-center">
                <h2>History</h2>
            </div>
            
        </div>
    </div>
   <div class="row">
        <a typpe="button" class="btn btn-success" href="{{ route('sales') }}">Back</a>
    </div>
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
   
    <table class="table table-bordered">
        <tr>
            <th>ID</th>
            <th>User</th>
            <th>Total Cost</th>
        </tr>
        
        @foreach ($orders as $order)

        <tr>
            <th>{{ $order->id }}</th>
            <th>{{ $order->name }}</th>
            <th>{{ $order->total_cost }}</th>
        </tr>
        @endforeach
    </table>
    
      
@endsection