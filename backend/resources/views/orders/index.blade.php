@extends('layouts.app')
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="d-flex justify-content-center">
                <h2>Orders</h2>
            </div>
            
        </div>
    </div>
   
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
   
    <table class="table table-bordered">
        <tr>
            <th>ID</th>
            <th>User</th>
            <th>Total Cost</th>

            <th width="280px">Action
                <div class="pull-right">
                <a class="btn btn-success" href="{{ route('orders.create') }}"> Create New Order</a>
            </div>
            </th>
        </tr>
        
        @foreach ($orders as $order)
        @if(Auth()->user()->name === $order->name)
        <tr>
            <td>{{ $order->id }}</td>
            <td>{{ $order->name }}</td>
            <td>{{ $order->total_cost}}</td>
            <td>
                <a class="btn btn-primary" href="{{ route('orders.show',$order->id) }}">Show</a>
            </td>
        </tr>
        @endif
        @endforeach
    </table>

      
@endsection