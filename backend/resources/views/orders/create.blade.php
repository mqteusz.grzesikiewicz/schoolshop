@extends('layouts.app')
@section('content')
@php
@endphp
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="d-flex justify-content-center">
            <h2>New Order</h2>
        </div>
        <div class="pull-right">
            <a class="btn btn-primary" href="/home"> Back</a>
        </div>
    </div>
</div>
   
@if ($errors->any())
    <div class="alert alert-danger">
        <strong>Whoops!</strong> There were some problems with your input.<br><br>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
{!! Form::open(array('route'=>'orders.store','method'=>'POST')) !!}   
<div class='row'>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Products:</strong>
           @foreach($products as $product)
           <label class="col-12">{{ Form::checkbox('products[]',$product->id, false, array('class'=>'name')) }}
                {{ $product->name}}
           </label>
           {{ Form::number($product->id, '1', array('class'=>'count')) }}
           @endforeach
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 text-center">
        <button type="submit" class="btn btn-primary">Submit</button>
    </div>
</div>
{!! Form::close() !!}
@endsection