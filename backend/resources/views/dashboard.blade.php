
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    @if(Auth()->user())
                    <div class="card-item">
                        <a type="button" class="btn btn-light" href="{{  route('orders.index')  }}"><h3>Your orders</h3></a>
                        Manage your orders
                    </div>
                    @endif
                   @role('admin')
                    <div class="card-item">
                        <a type="button" class="btn btn-light" href="{{ route('sales') }}"><h3>Sales</h3></a>
                        Check full schoop sales
                    </div>
                   @endrole
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
